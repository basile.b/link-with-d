module dside;

import core.stdc.string;
import std.string : toStringz;

extern(C):

const(char)* hello(const(char)* name)
{
    //throw new Exception("bla");
    return ("hello " ~ name[0 .. name.strlen]).toStringz;
}

struct Entry
{
    const char[]  name;
    const int     age;
}

Entry* makeEntry(char* name, int age)
{
    return new Entry(name[0 .. name.strlen].dup, age);
}

ubyte[4][] mapCRC32(char** names, int length)
{
    import std.algorithm.iteration : map;
    import std.digest.crc : crc32Of;
    import std.array : array;

    const base = names;

    struct PPCharRange
    {
        void popFront() { names += size_t.sizeof; }
        char[] front()  { return (*names)[0 .. (*names).strlen]; }
        bool empty()    { return ((names - base) / size_t.sizeof) >= length; }
    }

    PPCharRange r;
    return r.map!(a => a.crc32Of()).array;
}

