unit main;

{$mode objfpc}{$H+}
{$Assertions ON}
{$MODESWITCH ADVANCEDRECORDS}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

  // as per ABI specification
  generic TDArray<T> = record
  type
    PT = ^T;
  public
    length: PtrUInt;
    ptr:    PT;
  end;

  TDString = specialize TDArray<char>;

  TCRC32 = array[0..3] of Byte;
  TMapedCRC32 = specialize TDArray<TCRC32>;

  TEntry = record
    name: TDString;
    age:  integer;
  end;
  PEntry = ^TEntry;

// necessary to start the GC, run the static constructors, etc
procedure rt_init(); cdecl; external;
// cleanup
procedure rt_term(); cdecl; external;
// simple call
function hello(const name: PChar): PChar; cdecl; external;
// important: by the D side this one copies memory to the D GC pool
function makeEntry(name: PChar; age: integer): PEntry; cdecl; external;
// now lets use code more friendly to write in D...
function mapCRC32(names: PPChar; length: integer): TMapedCRC32; cdecl; external;

var
  Form1: TForm1;

{$LINKLIB libphobos2}
{$L dside.a}

implementation
{$R *.lfm}

procedure TForm1.FormCreate(Sender: TObject);
var
  e: PEntry;
  n: PPChar;
  m: TMapedCRC32;
  c: TCRC32;
  i: integer;
begin
  label1.Caption := hello(PChar(GetEnvironmentVariable('USER')));
  e := makeEntry(PChar('Douglas'), 19);
  assert(e^.age = 19);
  // we can slice ptr like in D, would look better with the {$mode delphi}
  assert(e^.name.ptr[0 .. e^.name.length] = 'Douglas');

  n := ArrayStringToPPchar(['Peter', 'Sophia', 'Douglas'], 3);
  m := mapCRC32(n, 3);
end;

initialization
  rt_init();
finalization
  rt_term();
end.

