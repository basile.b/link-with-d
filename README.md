# Link With D

Simple example showing how to use the [D language](https://dlang.org) in a FreePascal program.

## The D side

In the present case the D side is automatically compiled before the Lazarus project compilation starts.
This produces a static library file.

The D code must be `extern(C)`.

## The Pascal side

In order to use the D standard library or even its runtime we must link with phobos, which contains both.
This is done using the following directive:

```pascal
{$LINKLIB libphobos2}  
```

Then the D library produced when compilation starts must also be linked:

```pascal
{$L dside.a}
```

The D `extern(C)` declarations must have their `cdecl; extern;` Pascal counterparts.

## Notes

A possible error can be encountered when using different compilers.
The `{$LINKLIB libphobos2}` directive has for effect to link with the library produced by DMD itself 
so the D code used in the program must also be compiled with DMD and preferably the same version, 
as the D ABI is not stable (mangling scheme can change from a version to another for example).

If another D compiler is used then the `{$LINKLIB}` argument must be the standard library file shiped with this other compiler.

This demo was made under linux. Under Windows linking can be less obvious because the path to _libphobos2.lib_ is not standard.

## Why using D in a FreePascal program ?

D is more powerful but has not its _Lazarus_.
